package com.ts.ParkNest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParkNestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParkNestApplication.class, args);
	}

}
