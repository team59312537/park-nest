import { Component } from '@angular/core';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent {
  username: string = '';
  password: string = '';

  onLogin() {
    // Implement your login logic here, e.g., send credentials to an authentication API.
    console.log('Logging in with:', this.username, this.password);
  }
}
